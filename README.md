# Symfony UX Chartjs

Symfony UX Chartjs is a Symfony bundle integrating the Chartjs library in 
Symfony applications. It relies on the tools of the Symfony UX initiative to work.

Symfony UX Chartjs is currently considered **experimental**.

## Installation

Symfony UX Chartjs requires PHP 7.1+ and Symfony 5.0+.

You can install this bundle using Composer and Symfony Flex:

```sh
composer require symfony/ux-chartjs

# Don't forget to install the JavaScript dependencies as well and compile
yarn install --force
yarn dev
```

## Usage

The main usage of Symfony UX Chartjs is to use its *ChartBuilder* to create charts in PHP:

```php
// ...
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class HomeController extends AbstractController
{
    /**
     * @Route("", name="homepage")
     */
    public function index(ChartBuilderInterface $chartBuilder): Response
    {
        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            'datasets' => [
                [
                    'label' => 'My First dataset',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => [0, 10, 5, 2, 20, 30, 45],
                ],
            ],
        ]);
        
        $chart->setOptions([/* ... */]);

        return $this->render('home/index.html.twig', [
            'chart' => $chart,
        ]);
    }
}
```

The data and options defined here are provided as-is to Chart.js. You can read 
[their documentation](https://www.chartjs.org/docs/latest/) to discover all the configuration options.

Once created in PHP, a chart can be displayed using Twig:

```twig
{{ render_chart(chart) }}

{# You can pass HTML attributes as a second argument to add them on the <canvas> tag
{{ render_chart(chart, {'class': 'my-chart'}) }}
```

### Reacting to chart events

Symfony UX Chartjs dispatches JavaScript events when important things happen to the chart.
To react to these events, you can use a custom Stimulus controller:

```js
// mychart_controller.js

import { Controller } from 'stimulus';

export default class extends Controller {
    connect() {
        this.element.addEventListener('chartjs:load', this._onLoad);
        this.element.addEventListener('chartjs:hover', this._onHover);
        this.element.addEventListener('chartjs:click', this._onClick);
    }

    disconnect() {
        // You should always remove listeners when the controller is disconnected to avoid side effects
        this.element.removeEventListener('chartjs:load', this._onLoad);
        this.element.removeEventListener('chartjs:hover', this._onHover);
        this.element.removeEventListener('chartjs:click', this._onClick);
    }

    _onLoad(event) {
        // The chart was just loaded
        console.log(event.detail.chart); // You can access the chart instance using the event details
    }

    _onHover(event) {
        // The user hovered the chart
        console.log(event.detail.chart); // You can access the chart instance using the event details
        console.log(event.detail.mouseEvent); // You can access the raw mouse event using the event details
    }

    _onClick(event) {
        // The user clicked the chart
        console.log(event.detail.chart); // You can access the chart instance using the event details
        console.log(event.detail.mouseEvent); // You can access the raw mouse event using the event details
    }
}
```

Then in your render call, add your controller as an HTML attribute:

```twig
{{ render_chart(chart, {'data-controller': 'mychart'}) }}
```

## Backward Compatibility promise

This bundle aims at following the same Backward Compatibility promise as the Symfony framework:
[https://symfony.com/doc/current/contributing/code/bc.html](https://symfony.com/doc/current/contributing/code/bc.html)

However it is currently considered **experimental**.

## Run tests

### PHP tests

```sh
php vendor/bin/phpunit
```

### JavaScript tests

```sh
cd assets
yarn test
```
