<?php

namespace Symfony\UX\Chartjs\Builder;

use Symfony\UX\Chartjs\Model\Chart;

class ChartBuilder implements ChartBuilderInterface
{
    public function createChart(string $type): Chart
    {
        return new Chart($type);
    }
}
