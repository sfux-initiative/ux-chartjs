<?php

namespace Symfony\UX\Chartjs\Builder;

use Symfony\UX\Chartjs\Model\Chart;

interface ChartBuilderInterface
{
    public function createChart(string $type): Chart;
}
